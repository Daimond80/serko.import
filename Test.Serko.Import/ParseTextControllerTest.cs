using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Serko.Import;
using Serko.Import.Controllers;
using Serko.Tools.Parsers;
using Serko.Tools.Parsers.Implementation;
using System.IO;
using Xunit;

namespace Test.Serko.Import
{
	public class ParseTextControllerTest
	{
		private readonly IOptions<AppSettings> _appSettings;
		private readonly ITextParser _textParser;

		const string SUCCESS_FILENAME = @"TestData/Text_SUCCESS.txt";
		const string SUCCESS_NO_COSTCENTR_FILENAME = @"TestData/Text_SUCCESS_NO_COSTCENTR.txt";
		const string FAIL_NO_TOTAL_FILENAME = @"TestData/Text_FAIL_NO_TOTAL.txt";
		const string FAIL_NO_EXPENCE_FILENAME = @"TestData/Text_FAIL_NO_EXPENCE.txt";
		const string FAIL_INCORRECT_TOTAL_FILENAME = @"TestData/Text_FAIL_INCORRECT_TOTAL.txt";
		const string FAIL_NEGATIVE_TOTAL_FILENAME = @"TestData/Text_FAIL_NEGATIVE_TOTAL.txt";
		const string FAIL_NO_CLOSE_TAG01_FILENAME = @"TestData/Text_FAIL_NO_CLOSE_TAG01.txt";
		const string FAIL_NO_CLOSE_TAG02_FILENAME = @"TestData/Text_FAIL_NO_CLOSE_TAG02.txt";

		const decimal GST = 0.15m;

		public ParseTextControllerTest()
		{
			_appSettings = Options.Create(new AppSettings { GST = GST });
			_textParser = new TextParser();
		}

		[Theory]
		[InlineData(0.2)]
		[InlineData(0.15)]
		public void GetReservationEntry_DifferentGST_Success(decimal gst)
		{
			// Theory
			var innerAppSettings = Options.Create(new AppSettings { GST = gst });

			// Arrange
			var controller = new ParseTextController(innerAppSettings, _textParser);
			var text = File.ReadAllText(SUCCESS_FILENAME);

			// Act
			var result = controller.GetReservationEntry(text).Result as OkObjectResult;

			// Assert	
			Assert.NotNull(result);
			var m = Assert.IsAssignableFrom<ReservationEntryResult>(result.Value);

			Assert.True(m.Success);
			Assert.NotNull(m.Data);
			Assert.Equal("Tuesday 27 April 2017", m.Data.DateAsText);
			Assert.Equal("development team�s project end celebration dinner", m.Data.Description);
			Assert.Equal("Viaduct Steakhouse", m.Data.Vendor);
			Assert.NotNull(m.Data.Expense);
			Assert.Equal("DEV002", m.Data.Expense.CostCentre);
			Assert.Equal("personal card", m.Data.Expense.PaymentMethod);
			Assert.Equal(1024.01m, m.Data.Expense.Total);
			Assert.Equal(1024.01m * (1 - gst), m.Data.Expense.TotalExcludingGST);
			Assert.Equal(1024.01m * gst, m.Data.Expense.TotalGST);

		}

		[Fact]
		public void GetReservationEntry_Success_No_Costcentr()
		{
			// Arrange
			var controller = new ParseTextController(_appSettings, _textParser);
			var text = File.ReadAllText(SUCCESS_NO_COSTCENTR_FILENAME);

			// Act
			var result = controller.GetReservationEntry(text).Result as OkObjectResult;

			// Assert	
			Assert.NotNull(result);
			var m = Assert.IsAssignableFrom<ReservationEntryResult>(result.Value);

			Assert.True(m.Success);
			Assert.NotNull(m.Data);
			Assert.Equal("Tuesday 27 April 2017", m.Data.DateAsText);
			Assert.Equal("development team�s project end celebration dinner", m.Data.Description);
			Assert.Equal("Viaduct Steakhouse", m.Data.Vendor);
			Assert.NotNull(m.Data.Expense);

			Assert.Equal("UNKNOWN", m.Data.Expense.CostCentre);

			Assert.Equal("personal card", m.Data.Expense.PaymentMethod);
			Assert.Equal(1024.01m, m.Data.Expense.Total);
			var gst = _appSettings.Value.GST;
			Assert.Equal(1024.01m * (1 - gst), m.Data.Expense.TotalExcludingGST);
			Assert.Equal(1024.01m * gst, m.Data.Expense.TotalGST);

		}

		[Fact]
		/// For checking case when total = "-123"
		public void GetReservationEntry_Fail_Negative_Total()
		{
			// Arrange
			var controller = new ParseTextController(_appSettings, _textParser);
			var text = File.ReadAllText(FAIL_NEGATIVE_TOTAL_FILENAME);

			// Act
			var result = controller.GetReservationEntry(text).Result as BadRequestObjectResult;

			// Assert	
			Assert.NotNull(result);
			var m = Assert.IsAssignableFrom<ReservationEntryResult>(result.Value);

			Assert.False(m.Success);
			Assert.Null(m.Data);
			Assert.Equal("total: must be greater than or equal to zero", m.Error);
		}

		[Fact]
		/// For checking case when total = "abc"
		public void GetReservationEntry_Fail_Incorrect_Total()
		{
			// Arrange
			var controller = new ParseTextController(_appSettings, _textParser);
			var text = File.ReadAllText(FAIL_INCORRECT_TOTAL_FILENAME);

			// Act
			var result = controller.GetReservationEntry(text).Result as BadRequestObjectResult;

			// Assert	
			Assert.NotNull(result);
			var m = Assert.IsAssignableFrom<ReservationEntryResult>(result.Value);

			Assert.False(m.Success);
			Assert.Null(m.Data);
			Assert.Equal("total: Can not convert abc to System.Decimal", m.Error);

		}

		[Fact]
		public void GetReservationEntry_Fail_NoExpense()
		{
			// Arrange
			var controller = new ParseTextController(_appSettings, _textParser);
			var text = File.ReadAllText(FAIL_NO_EXPENCE_FILENAME);

			// Act
			var result = controller.GetReservationEntry(text).Result as BadRequestObjectResult;

			// Assert	
			Assert.NotNull(result);
			var m = Assert.IsAssignableFrom<ReservationEntryResult>(result.Value);

			Assert.False(m.Success);
			Assert.Null(m.Data);
			Assert.Equal("expense: Required", m.Error);
		}

		[Fact]
		public void GetReservationEntry_Fail_NoTotal()
		{
			// Arrange
			var controller = new ParseTextController(_appSettings, _textParser);
			var text = File.ReadAllText(FAIL_NO_TOTAL_FILENAME);

			// Act
			var result = controller.GetReservationEntry(text).Result as BadRequestObjectResult;

			// Assert	
			Assert.NotNull(result);
			var m = Assert.IsAssignableFrom<ReservationEntryResult>(result.Value);

			Assert.False(m.Success);
			Assert.Null(m.Data);
			Assert.Equal("total: Required", m.Error);
		}

		[Theory]
		[InlineData(FAIL_NO_CLOSE_TAG01_FILENAME, "vendor")]
		[InlineData(FAIL_NO_CLOSE_TAG02_FILENAME, "total")]
		public void GetReservationEntry_Fail_NoCloseTag(string textToTest, string missingTagName)
		{
			// Arrange
			var controller = new ParseTextController(_appSettings, _textParser);
			var text = File.ReadAllText(textToTest);

			// Act
			var result = controller.GetReservationEntry(text).Result as BadRequestObjectResult;

			// Assert	
			Assert.NotNull(result);
			var m = Assert.IsAssignableFrom<ReservationEntryResult>(result.Value);

			Assert.False(m.Success);
			Assert.Null(m.Data);
			Assert.Equal($"{missingTagName}: close tag not found", m.Error);
		}
	}
}
