﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Serko.Tools.Parsers;
using Serko.Tools.Parsers.Entries;

namespace Serko.Import.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ParseTextController : ControllerBase
	{
		/// Information:
		/// 
		/// As you can see I put GST value into .json-confguration of application.
		/// Really, I don't think that it is a good idea, because GST is very stable constant and any 
		/// unintentional changes of it could be cause of big bussiness problems. From that poin of view the better way is 
		/// hide it in the "const" variable in the TextParser class.
		/// So, lets say, it is only for show how configuration works in Core and some parts of DI
		/// 
		/// PS: From other side, if company wants to sell system to another countries (with different GTSs) then it has sense.
		private readonly AppSettings _appSettings;
		private readonly ITextParser _textParser;

		public ParseTextController(IOptions<AppSettings> appSettings, ITextParser textParser)
		{
			this._appSettings = appSettings.Value;
			this._textParser = textParser;
		}

		// GET api/values
		[HttpGet("GetReservationEntry")]
		public ActionResult<ReservationEntryResult> GetReservationEntry([FromBody]string text)
		{
			if (string.IsNullOrEmpty(text))
				return BadRequest("No text given");

			var result = new ReservationEntryResult();

			try
			{
				result.Data = _textParser.GetReservationEntry(text, _appSettings.GST);
				result.Success = true;

				return Ok(result);
			}
			catch (SerkoParseException ex)
			{
				result.Success = false;
				result.Error = ex.Message;
				return BadRequest(result);
			}
			/// no catch (Exception ex) - I don't want to catch all exceptions here - it
			/// should be centralized in filter with logging and so on. But it is "ok" to catch special SerkoParseException
			/// exception and return corresponding result here. Globally we should decide with front-developers what 
			/// exception/error-policy will be better. Same about logic for result of API call - we can do like I did 
			/// (Result-object with Success and Data fields) or just return Data if success and BadRequest for any errors.  
		}
	}
}
