﻿using Serko.Tools.Parsers.Entries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Serko.Import
{
	public class CommonResult<T> where T : class, new()
	{
		public T Data { get; set; }

		public bool Success { get; set; }

		public string Error { get; set; }
	}

	public class ReservationEntryResult : CommonResult<ReservationEntry>
	{
	}
}
