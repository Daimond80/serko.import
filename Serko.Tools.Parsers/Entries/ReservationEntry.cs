﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Xml.Serialization;

namespace Serko.Tools.Parsers.Entries
{
	public class ReservationEntry
	{
		[Required]
		[XmlElement("expense")]
		public ExpenseEntry Expense { get; set; }

		[XmlElement("vendor")]
		public string Vendor { get; set; }

		[XmlElement("description")]
		public string  Description { get; set; }

		[XmlElement("date")]
		public string DateAsText { get; set; }
	}
}
