﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Xml.Serialization;

namespace Serko.Tools.Parsers.Entries
{
	public class ExpenseEntry
	{
		[XmlElement("cost_centre")]
		public string CostCentre { get; set; }

		[XmlElement("payment_method")]
		public string PaymentMethod { get; set; }

		[Required]
		[XmlElement("total")]
		public decimal Total { get; set; }

		public decimal TotalGST { get; set; }

		/// I don't like calculatable properties in Model, but this is very very simple and clear case
		/// and I decide that it might be ok
		public decimal TotalExcludingGST { get => Total - TotalGST; }

		public ExpenseEntry()
		{
			//Missing <cost_centre>. In this case the ‘cost centre’ field in the output should be defaulted to ‘UNKNOWN’.
			// (could be done by using notation attributes too, but I decided to do it more simple)
			CostCentre = "UNKNOWN";
		}
	}
}
