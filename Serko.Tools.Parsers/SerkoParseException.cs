﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Serko.Tools.Parsers
{

	[Serializable]
	public class SerkoParseException : ApplicationException
	{
		public SerkoParseException() { }
		public SerkoParseException(string message) : base(message) { }
		public SerkoParseException(string message, Exception inner) : base(message, inner) { }
		protected SerkoParseException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}
