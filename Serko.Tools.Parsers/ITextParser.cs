﻿using Serko.Tools.Parsers.Entries;
using System;

namespace Serko.Tools.Parsers
{
	public interface ITextParser
	{
		ReservationEntry GetReservationEntry(string text, decimal gst);
	}
}
