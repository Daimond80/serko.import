﻿using Serko.Tools.Parsers.Entries;
using System;
using System.Reflection;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace Serko.Tools.Parsers.Implementation
{
	/// Actually, in that small exercise interview-application there are no sence to make interface ITextParser for TextParser.
	/// And communicate with TextParser via interface too.
	/// Why I did this is for the "future" purposes. For some future cases where we will use 
	/// TextParser.GetReservationEntry in huge method (where will be more other logic) and where we have to mock it for unit-test purposes.
	/// 
	public class TextParser : ITextParser
	{
		/// <summary>
		/// Extract Reservation Entry from text and calculate GST
		/// </summary>
		/// <param name="text">Text</param>
		/// <param name="gst">GST</param>
		/// <returns>Reservation Entry</returns>
		public ReservationEntry GetReservationEntry(string text, decimal gst)
		{
			var result = CreateEntry<ReservationEntry>(text);

			if (result.Expense.Total < 0)
				throw new SerkoParseException("total: must be greater than or equal to zero");

			result.Expense.TotalGST = result.Expense.Total * gst;

			return result;
		}

		/// <summary>
		/// Just user-friendly version of method "object CreateEntry(string text, Type type)"
		/// with typization
		/// </summary>
		/// <typeparam name="T">Type of the Entry need to be extracted</typeparam>
		/// <param name="text">Text</param>
		/// <returns>Entry</returns>
		public T CreateEntry<T>(string text)
			where T : class, new()
		{
			return (T)CreateEntry(text, typeof(T));
		}

		/// <summary>
		/// Core tools method for parsing text and creating objects according given type
		/// </summary>
		/// <param name="text">Text</param>
		/// <param name="type">Type of the Entry need to be extracted</param>
		/// <returns>Entry</returns>
		protected object CreateEntry(string text, Type type)
		{
			var result = Activator.CreateInstance(type);
			
			foreach (var prop in type.GetTypeInfo().GetProperties())
			{
				var name = prop.GetCustomAttribute<XmlElementAttribute>()?.ElementName;
				var required = prop.GetCustomAttribute<RequiredAttribute>() != null;

				if (string.IsNullOrEmpty(name))
					continue;

				const string REGEX_PATTERN = @"<{0}>(.*?)((<\/{0}>)|(\Z))";

				// Set IgnoreCase for any case
				Regex rgx = new Regex(string.Format(REGEX_PATTERN, name), RegexOptions.IgnoreCase | RegexOptions.Singleline);

				var m = rgx.Match(text);

				if (m.Success) // Perfect: we have found xml-tag
				{
					// Check if close tag present
					if (string.IsNullOrEmpty(m.Groups[2].Value))
						// Opening tags that have no corresponding closing tag. In this case the whole message should be rejected.
						throw new SerkoParseException($"{name}: close tag not found");

					// Check if it is simple type 
					if (prop.PropertyType.IsValueType || prop.PropertyType == typeof(string))
					{
						var res = m.Groups[1].Value.Trim(Environment.NewLine.ToCharArray());
						var typeConverter = TypeDescriptor.GetConverter(prop.PropertyType);

						try
						{
							var val = typeConverter.ConvertFromInvariantString(res);
							prop.SetValue(result, val);
						}
						catch (ArgumentException ex)
						{
							throw new SerkoParseException($"{name}: Can not convert {res} to {prop.PropertyType.ToString()}", ex);
						}
					}
					// ...or complex type
					else
					{
						// for complex type call same method recursively
						var val = CreateEntry(m.Groups[1].Value, prop.PropertyType);
						prop.SetValue(result, val);
					}
				}
				else
				{
					// when we haven't found xml-tag and property is mondatory => set it (and all parents) for null and stop procees
					if (required)
					{
						// Missing required tag. In this case the whole message should be rejected.
						//
						prop.SetValue(result, null);
						throw new SerkoParseException($"{name}: Required");
					}
				}
			}

			return result;
		}
	}
}
